<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manager Order History Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
    </script>
    <style>
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <h1>Manager Order History Page</h1>
      <nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="managerMain.html">Main</a></li>
          <li><a href="managerCreateEmployee.php">New Employee</a></li>
          <li><a href="managerCurrentEmployees.php">Current Employees</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Schedule<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerAvailability.php">Employee Availability</a></li>
              <li><a href="managerMasterSchedule.php">Master Schedule</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerInventory.php">Inventory Items</a></li>
              <li class="active"><a href="managerOrderHistory.php">Order History</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img src="images/sharkeyslogo.jpg" style="width:30%">
    </div>
    <?php
      require_once("db.php");
     //send a query to the database
     $sql = "select orderID, customerName, price from orders";
     $result = $mydb->query($sql);
     //$result should be a resultset
     echo "<h2>Order History</h2>
     <table border=1>
       <thead>
         <tr>
           <th>Order ID</th>
           <th>Customer Name</th>
           <th>Order Price</th>
         </tr>
       </thead>
       <tbody>";
     while($row = mysqli_fetch_array($result)){
       echo "<tr><td>".$row["orderID"]."</td><td>".$row["customerName"]."</td><td>".$row["price"]."</td></tr>";
     }

     echo "</tbody>
     </table>"

    ?>
  </body>
</html>
