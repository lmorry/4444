<?php
  $eShifts = 0;
  $eSDate = new DateTime();
  $eID = 0;
  $err = false;

  if (isset($_POST["submit"])) {
      if(isset($_POST["EShifts"])) $eShifts=$_POST["EShifts"];
      if(isset($_POST["ESDate"])) $eSDate=$_POST["ESDate"];
      if(isset($_POST["eID"])) $eID=$_POST["eID"];

      if(!empty($eShifts) && !empty($eID) && !empty($eSDate)) {
        header("HTTP/1.1 307 Temprary Redirect"); 
        header("Location: EmpHoursSubmit.php");
      } else {
        $err = true;
      }
  }
 ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee Availibilty</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
  </script>
  <title>Input Availabilty</title>
  <style>
    .errlabel {color:red;}
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}

    table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgba(242, 106, 7, 0.92);
    }
    td {
      height: 20px;
      color: black;
      background-color: lightyellow;

  </style>
</head>
<body>
<div class="container-fluid">
<h1>Input Shifts Availible to Work for the Week</h1>
<nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="employeeMain.html">Main</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Availibility<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="active"><a href="empHours.php">Input and View Availibility</a></li>
              <li><a href="deleteEmpAvail.php">Delete Availibilty</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="inventory.php">Add or Modify Inventory</a></li>
              <li><a href="inventoryTest.php">View and Sort Inventory Items</a></li>
              <li><a href="inventoryIndex.php">Inventory Bar Chart</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img id="img0" src="images/sharkeyslogo.jpg" style="width:30%">
      <br /><br />
      <div id="contentArea">&nbsp;</div>
    </div>
    <p>There are 2 Shifts per Day, 10am-4pm and 4pm-10pm</p>
    <p>You cannot input more than 14 available shifts for the week</p>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
    <label>Employee ID:</label>
    <input type="number" name="eID"/>
    <?php
      if ($err && empty($eID)) {
        echo "<label class='errlabel'>Error: Please enter Your Employee ID</label>";
      }
    ?>
    <br />
    <label>Select the Monday of Beginning Work Week</label>
    <input type="date" name="ESDate"/>
    <?php
      if ($err && empty($eSDate)) {
        echo "<label class='errlabel'>Error: Please enter Date</label>";
      }
    ?>
    <br />
    <label>Number of Shifts</label>
    <input type="number" name="EShifts"/>
    <?php
      if ($err && empty($eShifts)) {
        echo "<label class='errlabel'>Error: Please enter how may shifts you are available to work</label>";
      }
    ?>
    <br />
    <input type="submit" name="submit" value="Submit" />
    <br />
  </form>
</body>
</html>

