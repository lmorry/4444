<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manager Employees Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
    </script>
    <style>
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <h1>Manager Employees Page</h1>
      <nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="managerMain.html">Main</a></li>
          <li><a href="managerCreateEmployee.php">New Employee</a></li>
          <li class="active"><a href="managerCurrentEmployees.php">Current Employees</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Schedule<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerAvailability.php">Employee Availability</a></li>
              <li><a href="managerMasterSchedule.php">Master Schedule</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerInventory.php">Inventory Items</a></li>
              <li><a href="managerOrderHistory.php">Order History</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img src="images/sharkeyslogo.jpg" style="width:30%">

      <?php
    require_once("db.php");

    $sql="select EID, EUsername, EEmail, EShifts from Employee";

    $result = $mydb->query($sql);

    echo "
    <table border=1>
      <thead>
        <tr><th>Employee ID</th><th>Username</th><th>Email</th><th>Shifts</th></tr>
      </thead>
      <tbody>";

    while($row = mysqli_fetch_array($result)){
      //process each of the database records and put it into a <tr> element
      echo "<tr><td>".$row["EID"]."</td><td>".$row["EUsername"]."</td><td>".$row["EEmail"]."</td><td>".$row["EShifts"]."</td></tr>";
    }

    echo "
    </tbody>
    </table>";
  ?>

    </div>
  </body>
</html>
