<?php
  $orderID = 0;
  $customerName = "";
  $menuItem = "";
  $err = false;

  if (isset($_POST["update"])) {
    if(isset($_POST["orderID"])) $orderID = $_POST["orderID"];
    if(isset($_POST["customerName"])) $customerName = $_POST["customerName"];
    if(isset($_POST["menuItem"])) $menuItem = $_POST["menuItem"];


    if (!empty($customerName) && !empty($menuItem)){
      $err = false;
    } else {
      $err = true;
    }

    if(!$err){
        require_once("db.php");
        $sql = "update bit4444group02.orders set customerName='$customerName', 
        menuItem='$menuItem' where orderID = $orderID";
        echo $sql;
        $result=$mydb->query($sql);
    }
header("HTTP/1.1 307 Temprary Redirect");
header("Location: modifyitem.html");
}
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <title>Modify Order</title>
  <style>
    .errlabel {color:red;}
  </style>
</head>
<body>

<div class="container-fluid">
      <h1>Sharkey's Wing and Rib Joint</h1>
      <h2>Where Good Friends Go!</h2>
      <br />
      

      <!--navigation bar-->
      <nav>

        <ul class="nav nav-pills">
          <li class="active"><a href="homepage.html">Home</a></li>
          <li><a href="aboutus.html">About Us</a></li>
          <li><a href="menu.html">Menu</a></li>
          <li><a href="order.php">Order Online</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="customerLogin.php">Customer Login</a></li>
              <li><a href="employeeLogin.php">Employee Login</a></li>
              <li><a href="managerlogin.php">Manager Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      </div>

    </br>


    <h3>Modify Order</h3>
    <p>Please fill in all textboxes</p>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
    <label> Choose an Order ID: &nbsp;&nbsp;
    <select name="orderID" id="orderDropDown">
      <?php
        require_once("db.php");
        $sql = "select orderID from orders order by orderID asc";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["orderID"]."'>".$row["orderID"]."</option>";
        }
      ?>
    </select>
  </label><br />
  
  
    <label>Customer Name:
      <input type="text" name="customerName" value="<?php echo $customerName; ?>" />
      <?php
        if ($err && empty($customerName)) {
          echo "<label class='errlabel'>Error: Please enter a customer name.</label>";
        }
      ?>
    </label>
    <br />

    <label>Menu Item
    <select name="menuItem" id="menuDropDown" value="<?php echo $MenuItem;?>">
        <?php
        require_once("db.php");
        $sql = "select MenuItem from menu order by MenuID";
        
        $result = $mydb->query($sql);

        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["MenuItem"]."'>".$row["MenuItem"]."</option>";
        }
        
        ?>
      </select>
    </label> <br />

    <input type="submit" name="update" value="Submit" />
    </form>