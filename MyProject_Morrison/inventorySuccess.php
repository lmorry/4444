<!doctype html>
<html>
<head>
  <title>Inventory Input Success</title>
  <style type = "text/css">
      table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgba(242, 106, 7, 0.92);
    }
    td {
      height: 20px;
      color: black;
      background-color: lightyellow;
    }

            body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}

      
  </style>
</head>
<body>
    <?php
        $pID = 0;
        $pName = "";
        $quantity = 0; 
        $err = false;
      
        if(isset($_POST["inventoryID"])) $pID=$_POST["inventoryID"];
        if(isset($_POST["productname"])) $pName=$_POST["productname"];
        if(isset($_POST["quantity"])) $quantity=$_POST["quantity"];
    ?>

      <!-- display form data -->
    <p><strong><?php echo $pName; ?></strong> Has Been Added to Inventory with a Quantity of 
    <strong><?php echo $quantity;?></strong></p>

  <?php
    //add a new inventory record if inventoryID is new. otherwise, modify the existing inventory record
    require_once("db.php");

    if($pID=="") {
      $sql = "insert into inventory(ProductName, Quantity) values('$pName',$quantity)";
      
      $result=$mydb->query($sql);

      if ($result==1) {
        echo "<p>A new inventory record has been created</p>";
      }
    } else {
      //modify the inventory record...
        $sql = "update inventory set ProductName='$pName', Quantity=$quantity where InventoryID=$pID";
        $result=$mydb->query($sql);

        if ($result==1) {
          echo "<p>An inventory record has been updated</p>";
        }
    }
    $sql = "SELECT InventoryID, ProductName, Quantity FROM inventory";

    $result = $mydb->query($sql);

    echo "<table>";
    echo "<tr><th>InventoryID</th><th>ProductName</th><th>Quantity</th></tr>";

    while($row = mysqli_fetch_array($result)){
      echo "<tr>";

      echo '<td class=first>',$row["InventoryID"],'</td>';
      echo '<td>',$row["ProductName"],'</td>';
      echo '<td>',$row["Quantity"],'</td>';

      echo "</tr>";

    }
    echo "</table>"
   ?>
   <a href="inventory.php">Return to Add or Modify Item Page</a>
</body>
</html>