<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventory Bar Chart</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
    </script>
    <style>
          body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <h1>Inventory Bar Chart</h1>
      <nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="employeeMain.html">Main</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Availibility<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="empHours.php">Input and View Availibility</a></li>
              <li><a href="deleteEmpAvail.php">Delete Availibilty</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="inventory.php">Add or Modify Inventory</a></li>
              <li><a href="inventoryTest.php">View and Sort Inventory Items</a></li>
              <li class="active"><a href="inventoryIndex.php">Inventory Bar Chart</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img src="images/sharkeyslogo.jpg" style="width:30%">
    </div>
    <form action="inventoryShowProducts.php" method="get">
        <select name="InventoryID">
            <?php
              require_once("db.php");
              $sql = "select InventoryID, ProductName from inventory order by InventoryID";
              $result = $mydb->query($sql);
              while($row=mysqli_fetch_array($result)){
                  echo "<option value='".$row["InventoryID"]."'>".$row["ProductName"]."</option>";
              }
            ?>
        </select>
        <input type="submit" name="submit">
    </form>
  </body>
</html>
