<?php
  $MenuID = 0;
  $MenuItem = "";
  $MDescription = "";
  $MPrice = 0;
  $err = false;

  if (isset($_POST["submit"])) {
    if(isset($_POST["MenuID"])) $MenuID = $_POST["MenuID"];
    if(isset($_POST["MenuItem"])) $MenuItem = $_POST["MenuItem"];
    if(isset($_POST["MDescription"])) $MDescription = $_POST["MDescription"]; 
    if(isset($_POST["MPrice"])) $MPrice = $_POST["MPrice"];

    if (!empty($MenuID) && !empty($MenuItem)
    && !empty($MPrice) && !empty($MDescription) && $Mprice >= 0  ) {
    $err = false;   
    } else {
      $err = true;
    }
    if(!$err){
        require_once("db.php");
        $sql = "insert into bit4444group02.menu(MenuID, MenuItem, MDescription, MPrice)
        values($MenuID, '$MenuItem', '$MDescription', $MPrice)";
        echo $sql;
        $result = $mydb->query($sql);

    if($result == 1){
        echo '<script>alert:(You have successfully added an item to the menu...redirect to menu.")
        window.location.href = "menu.php";
        </script>';
    }
    header("HTTP/1.1 307 Temprary Redirect");
    header("Location: success.php");
    // else {
    //     echo '<script>alert("Please complete all fields to add item.")
    //     window.location.href = "additem.php";
    //     </script>';
    // }
  }
}
?>

<!doctype html>

<head>
  <title>Add New Menu Items</title>
  <style>
    .errlabel {color:red;}
    body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}
  </style>
</head>
<body>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>"
    <label>Menu ID:
      <input type="number" name="MenuID" value="<?php if(!empty($MenuID) && $MenuID>0) echo $MenuID; ?>" />
    </label>
    <br />

    <label>Menu Item:
      <input type="text" name="MenuItem" value="<?php echo $MenuItem; ?>" />
      <?php
        if ($err && empty($MenuItem)) {
          echo "<label class='errlabel'>Error: Please enter a Menu Item.</label>";
        }
      ?>
    </label>
    <br />

      </select>
    <br />
    <label>Menu Description:
      <input type="text" name="MDescription" value="<?php echo $MDescription; ?>" />
      <?php
        if ($err && empty($MDescription)) {
          echo "<label class='errlabel'>Error: Please enter a menu description.</label>";
        }
      ?>
    </label>
    <br />

      </select>

      <label>Menu Price:
      <input type="number" name="MPrice" value="<?php echo $MPrice; ?>" />
      <?php
        if ($err && ($MPrice=="" || $MPrice<0)) {
          echo "<label class='errlabel'>Error: Please enter a valid price.</label>";
        }
      ?>
    </label>
    <br />
    <input type="submit" name="submit" value="Submit" />
</body>
