<?php
$customerName = "";
$date = ""; 
$MenuItem = "";
//$MPrice = 0;
if (isset($_POST["submit"])) {
      if(isset($_POST["name"])) $customerName=$_POST["name"];
      if(isset($_POST["date"])) $date=$_POST["date"];
      if(isset($_POST["menuItem"])) $MenuItem=$_POST["menuItem"];
      //if(isset($_POST["menuPrice"])) $MPrice=$_POST["menuPrice"];

      if(!empty($customerName) && !empty($date) && !empty($MenuItem)) {
        header("HTTP/1.1 307 Temprary Redirect"); //
        header("Location: checkout.php");
      } else {
        $err = true;
      }
  }

?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <style>
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}
      </style>
<script>
    var asyncRequest;
  function getAllProducts(){

      try {
        	asyncRequest = new XMLHttpRequest();  //create request object

        	//register event handler
        	asyncRequest.onreadystatechange=stateChange;
            var url="menu.html";
        		asyncRequest.open('GET', url, true);  // prepare the request
                async
                Request.send();  // send the request NULL
        		}
        	catch (exception) {alert("Request failed");}
    }
    function stateChange(){
      if (asyncRequest.readyState == 4 && asyncRequest.status ==200){
        document.getElementById("contentArea").innerHTML = asyncRequest.responseText;
      }
    }
  function clearPage(){
    //the event handler for the mouseout event of the all products link
    document.getElementById("contentArea").innerHTML ="";
  }
  function init(){
    //set up the event handler for the mouseover, mouseout events on the all products link
    var over = document.getElementById("productLink")
    over.addEventListener("mouseover", getAllProducts)
    var out = document.getElementById("productLink")
    out.addEventListener("mouseout", clearPage)
  }
    //var c = document.getElementById("productLink");
    //c.addEventListener("mouseover", getAllProducts);
    //c.addEventListener("mouseout", clearPage);
  document.addEventListener("DOMContentLoaded", init);

  $(function(){
    $("#menuDropDown").change(function(){
      //var pid = document.forms[0].productid.value;
      $.ajax({
              url: "menu.html",
              async: true,
              success: function(result){
                $("#contentArea").html(result);
              }
            })
    })
  })
    </script>
</head>
<body>
<div class="container-fluid">
      <h1>Sharkey's Wing and Rib Joint</h1>
      <h2>Where Good Friends Go!</h2>
      <br />
      

      <!--navigation bar-->
      <nav>

        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="aboutus.html">About Us</a></li>
          <li><a href="menu.html">Menu</a></li>
          <li class="active"><a href="order.php">Order Online</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="customerLogin.php">Customer Login</a></li>
              <li><a href="employeeLogin.php">Employee Login</a></li>
              <li><a href="managerlogin.php">Manager Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>

</div>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
  <h2>Online Order Form</h2>
  <h3>View Popular Menu Items:</h3>
  <a href = "viewmenu.php">Click here to view popular menu items</a>
  <p>Provide Order Contact Information</p>
  <label>Name:</label></br>
  <input type="text" name="name" value="<?php echo $customerName; ?>" /> </br>

  
  <label>Date: (YEAR-MONTH-DAY HR:MIN:SEC)</label> </br>
  <input type="datetime" name="date" value="<?php echo $date; ?>" /></br> 

  <p>Select items to place in cart</p>

    <label> Choose a Menu Item: &nbsp;&nbsp;
      <select name="menuItem" id="menuDropDown" value="<?php echo $MenuItem;?>">
        <?php
        require_once("db.php");
        $sql = "select MenuItem from menu order by MenuID";
        
        $result = $mydb->query($sql);

        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["MenuItem"]."'>".$row["MenuItem"]."</option>";
        }
        
        ?>
      </select>
    </label> <br />
    <a href = "searchmenu.php">Search menu item price here</a> </br>

       <input type="submit" name="submit" value="Submit"/>
     </form>


</body>
</html>


