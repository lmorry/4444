<!doctype html>
<html>
<head>
  <title>Input Shift Success</title>
  <style type = "text/css">
    table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgba(242, 106, 7, 0.92);
    }
    td {
      height: 20px;
      color: black;
      background-color: lightyellow;
    }
    body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}

      
  </style>
</head>
<body>
    <?php
      $eShifts = 0;
      $eSDate = new DateTime();
      $eID = 0;
      $err = false;
          if(isset($_POST["EShifts"])) $eShifts=$_POST["EShifts"];
          if(isset($_POST["ESDate"])) $eSDate=$_POST["ESDate"];
          if(isset($_POST["eID"])) $eID=$_POST["eID"];
    ?>

      <!-- display form data -->
    <p>You have inputted <strong><?php echo $eShifts;?></strong> availible shifts on the week of <strong><?php echo $eSDate;?></strong></p>

  <?php
    //modify the existing employee record
    require_once("db.php");

      $sql = "update employee set EShifts=$eShifts, ESDate='$eSDate' where EID=$eID";

      $result=$mydb->query($sql);

    $sql = "SELECT EID, EUsername, EShifts, ESDate FROM employee";

    $result = $mydb->query($sql);

    echo "<table>";
    echo "<tr><th>Employee ID</th><th>Username</th><th>Shifts Availible</th><th>Week</th></tr>";

    while($row = mysqli_fetch_array($result)){
      echo "<tr>";

      echo '<td class=first>',$row["EID"],'</td>';
      echo '<td>',$row["EUsername"],'</td>';
      echo '<td>',$row["EShifts"],'</td>';
      echo '<td>',$row["ESDate"],'</td>';

      echo "</tr>";

    }
    echo "</table>"
   ?>
   <a href="empHours.php">Return to Availibilty Page</a>
</body>
</html>