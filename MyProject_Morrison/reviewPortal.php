<?php
  $RID = 0;
  $CID = 0;
  $Description = "";
  $Rating = 0;
  $err = false;

  if(isset($_POST["submit"])) {
    if(isset($_POST["RID"])) $RID = $_POST["RID"];
    if(isset($_POST["CID"])) $CID = $_POST["CID"];
    if(isset($_POST["Description"])) $Description = $_POST["Description"];
    if(isset($_POST["Rating"])) $Rating = $_POST["Rating"];


    if (!empty($RID) && !empty($CID) && !empty($Description) && !empty($Rating)){
      $err = false; 
      header("HTTP/1.1 307 Temprary Redirect");
      header("Location: customerPortal.php");
    } else {
      $err = true;
    }

    if(!$err){
        require_once("db.php");
        $sql = "insert into bit4444group02.review(RID, CID, Description, Rating) values($RID, $CID, 
        '$Description', $Rating)";
        echo $sql;
        $result=$mydb->query($sql);
    }

  }
?>

<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <title>Customer Login</title>
  <style>
    .errlabel {color:red;}
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
  </style>
  <script>
    $(function(){
      $("#productDropDown").change(function(){
        $.ajax({
          url:"displayReviews.php?CID=" + $("#productDropDown").val(),
          async:true,
          success: function(result){
            $("#contentArea").html(result);
          }
        })
      })
    })
	</script>
</head>
<body>
<div class="container-fluid">
      <h1>Sharkey's Wing and Rib Joint</h1>
      <h2>Where Good Friends Go!</h2>
      <br />
      

      <!--navigation bar-->
      <nav>

        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="aboutus.html">About Us</a></li>
          <li><a href="menu.html">Menu</a></li>
          <li><a href="order.php">Order Online</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="customerLogin.php">Customer Login</a></li>
              <li><a href="employeeLogin.php">Employee Login</a></li>
              <li><a href="managerlogin.php">Manager Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      </div>

    </br>


    <ul>
          <li><a href="customerAccountManagement.php">Account Management</a></li>
          <li><a href="reviewPortal.php">Review Portal</a></li>
          <li><a href="customerLogin.php">Log Out</a></li>
    </ul>

<h3>Create New Review</h3>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<label>Review ID:
      <input type="text" name="RID" value="<?php if(!empty($RID) && $RID>0) echo $RID; ?>" />
    </label>
    <br />

<label>Customer ID:
      <input type="text" name="CID" value="<?php if(!empty($CID) && $CID>0) echo $CID; ?>" />
    </label>
    <br />
  
  
<label>Description:
      <input type="text" name="Description" value="<?php echo $Description; ?>" />
      <?php 
        if ($err && empty($Description)) {
          echo "<label class='errlabel'>Error: Please enter a description.</label>";
        }
      ?>
</label>
<br />

<label>Rating out of 5:
      <input type="text" name="Rating" value="<?php if(!empty($Rating) && $Rating>0) echo $Rating; ?>" />
    </label>
<br />
<input type="submit" name="submit" value="Submit" />
</form>

<form method="get" action="showReviews.php">
<h3>Review History</h3>
<label> Choose a Customer ID: &nbsp;&nbsp;
    <select name="CID" id="productDropDown">
      <?php
        require_once("db.php");
        $sql = "select distinct CID from review";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["CID"]."'>".$row["CID"]."</option>";
        }
      ?>
    </select>
  </label><br />
  <div id="contentArea">&nbsp;</div>
  <input name="data" type="submit" value="Get Data Visualization"/>

</form>

</body>
</html>