<?php
  $eID = 0;

  if (isset($_POST["delete"])) {
    if(isset($_POST["eID"])){ $eID = $_POST["eID"];
        require_once("db.php");
        $sql = "update employee set esdate = null, eshifts = null where EID=$eID";
        echo $sql;
        $result=$mydb->query($sql);
    }
    header("HTTP/1.1 307 Temprary Redirect");
    header("Location: deleteSuccess.html");;

    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Delete Employee Availibilty</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
  </script>
  <title>Delete Availabilty</title>
  <style>
    .errlabel {color:red;}
    table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgba(242, 106, 7, 0.92);
    }
    td {
      height: 20px;
      color: black;
      background-color: lightyellow;
    }

      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}
  </style>
</head>
<body>
<div class="container-fluid">
<h1>Delete Availibilty</h1>
<nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="employeeMain.html">Main</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Availibility<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="empHours.php">Input and View Availibility</a></li>
              <li class="active"><a href="deleteEmpAvail.php">Delete Availibilty</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="inventory.php">Add or Modify Inventory</a></li>
              <li><a href="inventoryTest.php">View and Sort Inventory Items</a></li>
              <li><a href="inventoryIndex.php">Inventory Bar Chart</a></li>
            </ul>
          </li>
        </ul>
      </nav>
      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img id="img0" src="images/sharkeyslogo.jpg" style="width:30%">
      <br /><br />
      <div id="contentArea">&nbsp;</div>
    </div>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<label> Choose Your Employee ID: &nbsp;&nbsp;
    <select name="eID" id="productDropDown">
      <?php
        require_once("db.php");
        $sql = "select eid from employee order by eid";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["eid"]."'>".$row["eid"]."</option>";
        }
      ?>
    </select>
  </label><br />
  <div id="contentArea">&nbsp;</div>
<input name='delete' type='submit' value="Delete Availability" id="delete"/>
</form>
<?php
    $sql = "SELECT EID, EUsername, EShifts, ESDate FROM employee";

    $result = $mydb->query($sql);

    echo "<table>";
    echo "<tr><th>Employee ID</th><th>Username</th><th>Shifts Availible</th><th>Week</th></tr>";

    while($row = mysqli_fetch_array($result)){
      echo "<tr>";

      echo '<td class=first>',$row["EID"],'</td>';
      echo '<td>',$row["EUsername"],'</td>';
      echo '<td>',$row["EShifts"],'</td>';
      echo '<td>',$row["ESDate"],'</td>';

      echo "</tr>";

    }
    echo "</table>"
?>

</body>
</html>