<!doctype html>
<html>
<head>
  <title>Item added successfully</title>
  <style>
    table, td {
      border: 1px solid white;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th, td:first-child{
      color: white;
      background-color: rgb(166, 68, 12);
    }
    td {
      width: 15em;
      height: 20px;
      color: white;
      background-color: rgb(245, 211, 136);
    }
  </style>
</head>
<body>
  <?php
    $MenuID = 0;
    $MenuItem = "";
    $MDescription = "";
    $MPrice = 0;
    $err = false;

    if (isset($_POST["submit"])) {
        if(isset($_POST["MenuID"])) $MenuID = $_POST["MenuID"];
        if(isset($_POST["MenuItem"])) $MenuItem = $_POST["MenuItem"];
        if(isset($_POST["MDescription"])) $MDescription = $_POST["MDescription"]; 
        if(isset($_POST["MPrice"])) $MPrice = $_POST["MPrice"];

    require_once("db.php");

    if ($MenuID == 0) {
      $sql = "insert into menu(MenuID, MenuItem, MDescription,
              Mprice)
              values($MenuID, '$MenuItem', '$MDescription',
            $MPrice)";
      $result=$mydb->query($sql);
      if ($result==1) {
        echo "<p>A new product record has been added.</p>";
      }

      //For the newly added product record, the database will assign a
      //unique productID value. The code here is tring to query the database
      //for the latest ProductID
      $sql = "select max(MenuID) as MenuID from menu";
      $result = $mydb->query($sql);
      $row=mysqli_fetch_array($result);
      $productID = $row["MenuID"]; //maxpid is the column name in the sql result table
      
    } else {
      $sql = "update menu set MenuID=$MenuID, MenuItem='$MenuItem',
              MDescription='$MDescription', MPrice=$MPrice where MenuID=$MenuID";
      $result=$mydb->query($sql);

      if ($result==1) {
        echo "<p>A product record has been updated.</p>";
      }
    }
} 

    //display the record in a table format
    echo "<table>";
    echo "<thead>";
    echo "<th>Menu ID</th><th>Menu Item</th><th>Menu Decription</th><th>Menu Price</th>";
    echo "</thead>";
    echo "<tbody><td>$MenuID</td><td>$MenuItem</td><td>$MDescription</td><td>$MPrice</td>";
    echo "</tbody></table>";
  ?>
</body>
</html>