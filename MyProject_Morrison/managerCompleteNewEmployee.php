<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manager Complete New Employee</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
    </script>
    <style>
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
    </style>
</head>
<body>
  <?php
    $eUsername = "";
    $ePassword = "";
    $eEmail = "";
    $eShifts = 0;

    if(isset($_POST["eusername"])) $eUsername=$_POST["eusername"];
    if(isset($_POST["epassword"])) $ePassword=$_POST["epassword"];
    if(isset($_POST["eemail"])) $eEmail=$_POST["eemail"];
    if(isset($_POST["eshifts"])) $eShifts=$_POST["eshifts"];
  ?>

  <!-- display form data -->
  <div class="container-fluid">
      <h1>Manager Complete New Employee Page</h1>
      <nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="managerMain.html">Main</a></li>
          <li class="active"><a href="managerCreateEmployee.php">New Employee</a></li>
          <li><a href="managerCurrentEmployees.php">Current Employees</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Schedule<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerAvailability.php">Employee Availability</a></li>
              <li><a href="managerMasterSchedule.php">Master Schedule</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerInventory.php">Inventory Items</a></li>
              <li><a href="managerOrderHistory.php">Order History</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>
      <img src="images/sharkeyslogo.jpg" style="width:30%">

      <h2>New Employee Credentials</h2>
      <?php
        echo "<table border=1>
          <thead>
            <tr>
              <th>Employee Username</th>
              <th>Employee Email</th>
              <th>Employee Shifts</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>$eUsername</td>
              <td>$eEmail</td>
              <td>$eShifts</td>
            </tr>
          </tbody>
        </table>";
      ?>

  <?php
    //add a new employee record if emptype is new. otherwise, modify the existing employee record
    require_once("db.php");

    if(!empty($eUsername)) {
      $sql = "insert into employee(EUsername, EPassword, EEmail, EShifts) values('$eUsername', '$ePassword', '$eEmail', '$eShifts')";

      $result=$mydb->query($sql);

      if ($result==1) {
        echo "<p>A new employee record has been created</p>";
      }
    }
   ?>
</body>
</html>
