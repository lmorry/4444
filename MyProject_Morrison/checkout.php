<!doctype html>
<html>
<head>
  <title>Checkout Successful</title>
  <style>
    table, td {
      border: 1px solid white;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th, td:first-child{
      color: white;
      background-color: rgb(166, 68, 12);
    }
    td {
      width: 15em;
      height: 20px;
      color: white;
      background-color: rgb(245, 211, 136);
    }
  </style>
</head>
<body>
  <?php
    $customerName = "";
    $date = ""; 
    $MenuItem = "";
    $MPrice = 0;
    $err = false;
    $productID = 0;

        if(isset($_POST["menuItem"])) $MenuItem = $_POST["menuItem"];
        //if(isset($_POST["MPrice"])) $MPrice = $_POST["MPrice"];
        if(isset($_POST["name"])) $customerName = $_POST["name"];
        if(isset($_POST["date"])) $date = $_POST["date"];
?>
<?php
require_once("db.php");
  //For the newly added product record, the database will assign a
      //unique productID value. The code here is tring to query the database
      //for the latest ProductID
      $sql = "select max(orderID) as orderID from orders";
      $result = $mydb->query($sql);
      $row=mysqli_fetch_array($result);
      $productID = $row["orderID"] + 1; //maxpid is the column name in the sql result table
?>
  <?php
    //display the record in a table format
    echo "<table>";
    echo "<thead>";
    echo "<th>Order ID</th><th>Menu Item</th><th>Customer Name</th>
    <th>Date</th>";
    echo "</thead>";
    echo "<tbody><td>$productID</td><td>$MenuItem</td><td>$customerName</td>
    <td>$date</td>";
    echo "</tbody></table>";
  ?>

<?php
  require_once("db.php");


  //$menuItem = "";
  //if(isset($_GET["menuItem"])) $menuItem=$_GET["menuItem"];
  $sql = "select MPrice from menu where MenuItem='$MenuItem'";
  $result = $mydb->query($sql);
      $row=mysqli_fetch_array($result);
      echo "<p>Your order price is: ".$row['MPrice']."</p>"; 

      $MPrice = $row['MPrice'];
      if (!empty($customerName)) {
        $sql = "insert into orders(date, customerName, price, menuItem)
                values('$date', '$customerName', '$MPrice', '$MenuItem')";
        $result=$mydb->query($sql);
        if ($result==1) {
          echo "<p>You have checked out successfully.</p>";
        }
        
      } 
  ?> 
  <p>Thank you for your Sharkeys order.</p>
  <a href = modifyitem.php>Click here to modify order</a> </br>
  <a href = deleteitem.php>Click here to cancel order</a> </br>
  <a href= homepage.html>Click here to go back to the homepage</a>
</body>
</html>
