<?php
  $CID = 0;
  $CUsername = "";
  $CPassword = "";
  $CFname = "";
  $CLname = "";
  $CEmail = "";
  $CPhoneNumber = "";
  $CAddress = "";
  $err = false;

  if (isset($_POST["submit"])) {
    if(isset($_POST["CID"])) $CID = $_POST["CID"];
    if(isset($_POST["CUsername"])) $CUsername = $_POST["CUsername"];
    if(isset($_POST["CPassword"])) $CPassword = $_POST["CPassword"];
    if(isset($_POST["CFname"])) $CFname = $_POST["CFname"];
    if(isset($_POST["CLname"])) $CLname = $_POST["CLname"];
    if(isset($_POST["CEmail"])) $CEmail = $_POST["CEmail"];
    if(isset($_POST["CPhoneNumber"])) $CPhoneNumber = $_POST["CPhoneNumber"];
    if(isset($_POST["CAddress"])) $CAddress = $_POST["CAddress"];


    if (!empty($CID) && !empty($CUsername) && !empty($CPassword) && !empty($CFname) && !empty($CLname) &&
    !empty($CEmail) && !empty($CPhoneNumber) && !empty($CAddress)){
      $err = false;
      header("HTTP/1.1 307 Temprary Redirect");
      header("Location: customerPortal.php");
    } else {
      $err = true;
    }

    if(!$err){
        require_once("db.php");
        $sql = "insert into bit4444group02.customer(CID, CUsername, CPassword, CFname, CLname, CEmail, CPhoneNumber,
        CAddress) values($CID, '$CUsername', '$CPassword', '$CFname', '$CLname', '$CEmail', 
        '$CPhoneNumber', '$CAddress')";
        echo $sql;
        $result=$mydb->query($sql);
    }
    

    }
?>

<!doctype html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <title>Create a Customer Account</title>
  <style>
    .errlabel {color:red;}
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
  </style>
</head>
<body>
<div class="container-fluid">
      <h1>Sharkey's Wing and Rib Joint</h1>
      <h2>Where Good Friends Go!</h2>
      <br />
      

      <!--navigation bar-->
      <nav>

        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="aboutus.html">About Us</a></li>
          <li><a href="menu.html">Menu</a></li>
          <li><a href="order.php">Order Online</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="customerLogin.php">Customer Login</a></li>
              <li><a href="employeeLogin.php">Employee Login</a></li>
              <li><a href="managerlogin.php">Manager Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      </div>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
    <label>Customer ID:
      <input type="text" name="CID" value="<?php if(!empty($CID) && $CID>0) echo $CID; ?>" />
    </label>
    <br />
  
  
    <label>Username:
      <input type="text" name="CUsername" value="<?php echo $CUsername; ?>" />
      <?php
        if ($err && empty($CUsername)) {
          echo "<label class='errlabel'>Error: Please enter a username.</label>";
        }
      ?>
    </label>
    <br />

    <label>Password:
      <input type="password" name="CPassword" value="<?php echo $CPassword; ?>" />
      <?php
        if ($err && empty($CPassword)) {
          echo "<label class='errlabel'>Error: Please enter a password.</label>";
        }
      ?>
    </label>
    <br />

    <label>First Name:
      <input type="text" name="CFname" value="<?php echo $CFname; ?>" />
      <?php
        if ($err && empty($CFname)) {
          echo "<label class='errlabel'>Error: Please enter a first name.</label>";
        }
      ?>
    </label>
    <br />

    <label>Last Name:
      <input type="text" name="CLname" value="<?php echo $CLname; ?>" />
      <?php
        if ($err && empty($CLname)) {
          echo "<label class='errlabel'>Error: Please enter a last name.</label>";
        }
      ?>
    </label>
    <br />

    <label>Email:
        <input name="CEmail" type="text" value="<?php echo $CEmail; ?>"/>
        <?php
          if ($err && empty($CEmail)) {
            echo "<label class='errlabel'>Error: Please enter an email</label>";
          }
        ?>
    </label>
    <br />

    <label>Phone Number:
        <input name="CPhoneNumber" type="text" value="<?php echo $CPhoneNumber; ?>"/>
        <?php
          if ($err && empty($CPhoneNumber)) {
            echo "<label class='errlabel'>Error: Please enter a phone number.</label>";
          }
        ?>
    </label>
    <br />

    <label>Address:
        <input name="CAddress" type="text" value="<?php echo $CAddress; ?>"/>
        <?php
          if ($err && empty($CAddress)) {
            echo "<label class='errlabel'>Error: Please enter an address.</label>";
          }
        ?>
    </label>
    <br />

    <input type="submit" name="submit" value="Submit" />
    </form>
</body>
</html>