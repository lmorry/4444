<?php
  $pID = 0;
  $pName = "";
  $quantity = 0; 
  $err = false;

  if (isset($_POST["submit"])) {
      if(isset($_POST["inventoryID"])) $pID=$_POST["inventoryID"];
      if(isset($_POST["productname"])) $pName=$_POST["productname"];
      if(isset($_POST["quantity"])) $quantity=$_POST["quantity"];

      if($pID>=0 && !empty($pName) && $quantity>=0) {
        header("HTTP/1.1 307 Temprary Redirect"); 
        header("Location: inventorySuccess.php");
      } else {
        $err = true;
      }
  }
 ?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventory Add/Modify</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
  </script>
  <title>Add Product and View Inventory Levels</title>
  <style>
    .errlabel {color:red;}
table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgba(242, 106, 7, 0.92);
    }
    td {
      height: 20px;
      color: black;
      background-color: lightyellow;
    }

            body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}

  </style>
</head>

<body>
<div class="container-fluid">
<h1>Add or Modify Inventory Item</h1>
<nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="employeeMain.html">Main</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Availibility<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="empHours.php">Input and View Availibility</a></li>
              <li><a href="deleteEmpAvail.php">Delete Availibilty</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="active"><a href="inventory.php">Add or Modify Inventory</a></li>
              <li><a href="inventoryTest.php">View and Sort Inventory Items</a></li>
              <li><a href="inventoryIndex.php">Inventory Bar Chart</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img id="img0" src="images/sharkeyslogo.jpg" style="width:30%">
      <br /><br />
      <div id="contentArea">&nbsp;</div>
    </div>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
    <label>Product ID: (Leave Blank if Adding a New Inventory Item)</label>
    <input type="number" name="inventoryID" maxlength="6" size="3" />
    <br />

    <label>Product Name:</label>
    <input name="productname" type="text" value="<?php echo $pName; ?>"/>
    <?php
      if ($err && empty($pName)) {
        echo "<label class='errlabel'>Error: Please enter a product name</label>";
      }
    ?>
    <br />

    <label>Quantity</label>
    <input type="number" name="quantity" value="<?php echo $quantity; ?>"/>
    <?php
      if ($err && $quantity<0) {
        echo "<label class='errlabel'>Error: Please enter a valid quantity level</label>";
      }
    ?>
    <br />

    <input type="submit" name="submit" value="Submit" />
    <br />
  </form>
  <br/>
  <a href="inventoryTest.php">Click to View and Sort Inventory</a>
  <h2>View Current Inventory</h2>
  <?php
    require_once("db.php");
    $sql = "SELECT InventoryID, ProductName, Quantity FROM inventory";

    $result = $mydb->query($sql);

    echo "<table>";
    echo "<tr><th>InventoryID</th><th>ProductName</th><th>Quantity</th></tr>";

    while($row = mysqli_fetch_array($result)){
      echo "<tr>";

      echo '<td class=first>',$row["InventoryID"],'</td>';
      echo '<td>',$row["ProductName"],'</td>';
      echo '<td>',$row["Quantity"],'</td>';

      echo "</tr>";

    }
    echo "</table>"
   ?>


</body>

</html>