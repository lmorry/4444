<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
<title>Review Bar Chart</title>
<meta charset="utf-8">
<style>

.bar {
  fill: maroon;
}

.bar:hover {
  fill: orange;
}

.axis--x path {
  display: none;
}

      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}

</style>
</head>
<body>
<div class="container-fluid">
      <h1>Sharkey's Wing and Rib Joint</h1>
      <h2>Where Good Friends Go!</h2>
      <br />
      

      <!--navigation bar-->
      <nav>

        <ul class="nav nav-pills">
          <li ><a href="homepage.html">Home</a></li>
          <li><a href="aboutus.html">About Us</a></li>
          <li><a href="menu.html">Menu</a></li>
          <li><a href="orderonline.html">Order Online</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="customerLogin.php">Customer Login</a></li>
              <li><a href="employeeLogin.php">Employee Login</a></li>
              <li><a href="managerlogin.php">Manager Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>
      </div>
<h3>Review History Data</h3>
<h5>(Review ID vs Rating out of 5)</h5>
<svg width="960" height="500"></svg>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

var svg = d3.select("svg"),
    margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);
console.log(y(3));
var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//d3.tsv("data.tsv", function(d) {
  //d.frequency = +d.frequency;
  //console.log(d);
  //return d;

d3.json("getReviewData.php?CID=<?php
  if(isset($_GET['CID'])) 
    echo $_GET['CID'];
  else
    echo "0";

?>", function(error, data) {
  if (error) throw error;

  data.forEach(function(d){
    d.letter = d.RID;
    d.frequency = +d.Rating;
  })


  x.domain(data.map(function(d) { return d.letter; }));
  y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(4,"s"))
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Frequency");

  g.selectAll(".bar")
    .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.letter); })
      .attr("y", function(d) { return y(d.frequency); })
      .attr("width", x.bandwidth())
      .attr("height", function(d) { return height - y(d.frequency); });
});
</script>
</body>
</html>