<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manager Master Schedule Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
    </script>
    <style>
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
    </style>
</head>
<body>
  <?php
  $empShiftSundayAM = "";
  $empShiftSundayPM = "";
  $empShiftMondayAM = "";
  $empShiftMondayPM = "";
  $empShiftTuesdayAM = "";
  $empShiftTuesdayPM = "";
  $empShiftWednesdayAM = "";
  $empShiftWednesdayPM = "";
  $empShiftThursdayAM = "";
  $empShiftThursdayPM = "";
  $empShiftFridayAM = "";
  $empShiftFridayPM = "";
  $empShiftSaturdayAM = "";
  $empShiftSaturdayPM = "";

    if(isset($_POST["empShiftSundayAM"])) $empShiftSundayAM=$_POST["empShiftSundayAM"];
    if(isset($_POST["empShiftSundayPM"])) $empShiftSundayPM=$_POST["empShiftSundayPM"];
    if(isset($_POST["empShiftMondayAM"])) $empShiftMondayAM=$_POST["empShiftMondayAM"];
    if(isset($_POST["empShiftMondayPM"])) $empShiftMondayPM=$_POST["empShiftMondayPM"];
    if(isset($_POST["empShiftTuesdayAM"])) $empShiftTuesdayAM=$_POST["empShiftTuesdayAM"];
    if(isset($_POST["empShiftTuesdayPM"])) $empShiftTuesdayPM=$_POST["empShiftTuesdayPM"];
    if(isset($_POST["empShiftWednesdayAM"])) $empShiftWednesdayAM=$_POST["empShiftWednesdayAM"];
    if(isset($_POST["empShiftWednesdayPM"])) $empShiftWednesdayPM=$_POST["empShiftWednesdayPM"];
    if(isset($_POST["empShiftThursdayAM"])) $empShiftThursdayAM=$_POST["empShiftThursdayAM"];
    if(isset($_POST["empShiftThursdayPM"])) $empShiftThursdayPM=$_POST["empShiftThursdayPM"];
    if(isset($_POST["empShiftFridayAM"])) $empShiftFridayAM=$_POST["empShiftFridayAM"];
    if(isset($_POST["empShiftFridayPM"])) $empShiftFridayPM=$_POST["empShiftFridayPM"];
    if(isset($_POST["empShiftSaturdayAM"])) $empShiftSaturdayAM=$_POST["empShiftSaturdayAM"];
    if(isset($_POST["empShiftSaturdayPM"])) $empShiftSaturdayPM=$_POST["empShiftSaturdayPM"];
  ?>

  <!-- display form data -->
  <div class="container-fluid">
      <h1>Manager Master Schedule Page</h1>
      <nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="managerMain.html">Main</a></li>
          <li><a href="managerCreateEmployee.php">New Employee</a></li>
          <li><a href="managerCurrentEmployees.php">Current Employees</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Schedule<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerAvailability.php">Employee Availability</a></li>
              <li class="active"><a href="managerMasterSchedule.php">Master Schedule</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerInventory.php">Inventory Items</a></li>
              <li><a href="managerOrderHistory.php">Order History</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>
      <img src="images/sharkeyslogo.jpg" style="width:30%">
      <?php
        echo "<h2>AM Shifts</h2>
          <table border=1>
            <thead>
              <tr>
                <th>Sunday AM Shift (10:00 AM - 4:00PM)</th>
                <th>Monday AM Shift (10:00 AM - 4:00PM)</th>
                <th>Tuesday AM Shift (10:00 AM - 4:00PM)</th>
                <th>Wednesday AM Shift (10:00 AM - 4:00PM)</th>
                <th>Thursday AM Shift (10:00 AM - 4:00PM)</th>
                <th>Friday AM Shift (10:00 AM - 4:00PM)</th>
                <th>Saturday AM Shift (10:00 AM - 4:00PM)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$empShiftSundayAM</td>
                <td>$empShiftMondayAM</td>
                <td>$empShiftTuesdayAM</td>
                <td>$empShiftWednesdayAM</td>
                <td>$empShiftThursdayAM</td>
                <td>$empShiftFridayAM</td>
                <td>$empShiftSaturdayAM</td>
              </tr>
            </tbody>
          </table>
          <h2>PM Shifts</h2>
          <table border=1>
            <thead>
              <tr>
                <th>Sunday PM Shift (4:00 PM - 10:00PM)</th>
                <th>Monday PM Shift (4:00 PM - 10:00PM)</th>
                <th>Tuesday PM Shift (4:00 PM - 10:00PM)</th>
                <th>Wednesday PM Shift (4:00 PM - 10:00PM)</th>
                <th>Thursday PM Shift (4:00 PM - 10:00PM)</th>
                <th>Friday PM Shift (4:00 PM - 10:00PM)</th>
                <th>Saturday PM Shift (4:00 PM - 10:00PM)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$empShiftSundayPM</td>
                <td>$empShiftMondayPM</td>
                <td>$empShiftTuesdayPM</td>
                <td>$empShiftWednesdayPM</td>
                <td>$empShiftThursdayPM</td>
                <td>$empShiftFridayPM</td>
                <td>$empShiftSaturdayPM</td>
              </tr>
            </tbody>
          </table>";
      ?>


</body>
</html>
