<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventory View and Sort</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
  </script>
  <title>View Inventory</title>
  <style>
    table, th, td {
      border: 1px solid black;
    }
    table {
      border-collapse: collapse;
      empty-cells: show;
      display:
    }
    th {
      color: white;
      background-color: rgba(242, 106, 7, 0.92);
    }
    td {
      height: 20px;
      color: black;
      background-color: lightyellow;
    }
          body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}

  </style>
  <script src="jquery-3.1.1.min.js"></script>
  <script>
    //ajex in Javascript
		var asyncRequest;

    function getAllProducts() {
      //display all products
      var url = "inventoryTestDisplay.php";
        try {
          asyncRequest = new XMLHttpRequest();

          asyncRequest.onreadystatechange=stateChange;
          asyncRequest.open('GET',url,true);
          asyncRequest.send();
        }
          catch (exception) {alert("Request failed");}
    }

		function stateChange() {
			if(asyncRequest.readyState==4 && asyncRequest.status==200) {
				document.getElementById("contentArea").innerHTML=
					asyncRequest.responseText;
			}
		}

    function clearPage(){
      document.getElementById("contentArea").innerHTML = "";
    }

    function init(){

      var z3 = document.getElementById("productLink");
      z3.addEventListener("mouseover", getAllProducts);

      var z4 = document.getElementById("productLink");
      z4.addEventListener("mouseout", clearPage);
    }
    document.addEventListener("DOMContentLoaded", init);

    //ajax in jQuery
    $(function(){
      $("#productDropDown").change(function(){
        $.ajax({
          url:"inventoryTestDisplay.php?id=" + $("#productDropDown").val(),
          async:true,
          success: function(result){
            $("#contentArea").html(result);
          }
        })
      })
    })
	</script>
</head>
<body>
<div class="container-fluid">
<h1>View and Sort Inventory Items</h1>
<nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="employeeMain.html">Main</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Availibility<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="empHours.php">Input and View Availibility</a></li>
              <li><a href="deleteEmpAvail.php">Delete Availibilty</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="inventory.php">Add or Modify Inventory</a></li>
              <li class="active"><a href="inventoryTest.php">View and Sort Inventory Items</a></li>
              <li><a href="inventoryIndex.php">Inventory Bar Chart</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>

      <img id="img0" src="images/sharkeyslogo.jpg" style="width:30%">
      <br /><br />
      <div id="contentArea">&nbsp;</div>
    </div>
  <a id="productLink" href="">All Inventory</a> <br />
  <label> Choose an Inventory Item: &nbsp;&nbsp;
    <select name="inventoryid" id="productDropDown">
      <?php
        require_once("db.php");
        $sql = "select inventoryid, productname from inventory order by inventoryid";
        $result = $mydb->query($sql);
        while($row=mysqli_fetch_array($result)){
          echo "<option value='".$row["inventoryid"]."'>".$row["productname"]."</option>";
        }
      ?>
    </select>
  </label><br />
  <div id="contentArea">&nbsp;</div>
</body>
</html>