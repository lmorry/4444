<?php
  $username="";
  $password="";
  $remember="no";
  $error = false;
  $loginOK = null;

  if(isset($_POST["submit"])){
    if(isset($_POST["username"])) $username=$_POST["username"];
    if(isset($_POST["pwd"])) $password=$_POST["pwd"];
    if(isset($_POST["remember"])) $remember=$_POST["remember"];

    //echo ($username.".".$password.".".$remember);
    if(empty($username) || empty($password)) {
      $error=true;
    }

    //set cookies for remembering the user name
    if(!empty($username) && $remember=="yes"){
      setcookie("username", $username, time()+60*60*24, "/");
    }


    if(!$error){
      //check username and password with the database record
      require_once("db.php");
      $sql = "select CPassword from customer where CUsername='$username'";
      $result = $mydb->query($sql);

      $row=mysqli_fetch_array($result);
      if ($row){
        if(strcmp($password, $row["CPassword"]) ==0 ){
          $loginOK=true;
        } else {
          $loginOK = false;
        }
      }

      if($loginOK) {
        //set session variable to remember the username
        session_start();
        $_SESSION["username"] = $username;

        Header("Location:customerPortal.php");
      }
    }
  }


 ?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <script src="jquery-3.1.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <title>Customer Login</title>
  <style>
    .errlabel {color:red;}
      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills {color:orange}
      .nav-pills > li > a {color:maroon}
  </style>
</head>
<body>
<div class="container-fluid">
      <h1>Sharkey's Wing and Rib Joint</h1>
      <h2>Where Good Friends Go!</h2>
      <br />
      

      <!--navigation bar-->
      <nav>

        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="aboutus.html">About Us</a></li>
          <li><a href="menu.html">Menu</a></li>
          <li><a href="order.php">Order Online</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="active"><a href="customerLogin.php">Customer Login</a></li>
              <li><a href="employeeLogin.php">Employee Login</a></li>
              <li><a href="managerlogin.php">Manager Login</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      </div>
  <h1>Customer Login</h1>
  <img src="images/sharkeyslogo.jpg" style="width:20%">
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
    <table>
      <tr>
        <td>Username</td>
      </tr>
      <tr>
        <td><input type="text" name="username" value="<?php
          if(!empty($username))
            echo $username;
          else if(isset($_COOKIE['username'])) {
            echo $_COOKIE['username'];
          }
        ?>" /><?php if($error && empty($username)) echo "<span class='errlabel'> please enter a username</span>"; ?></td>
      </tr>
      <tr>
        <td>Password</td>
      </tr>
      <tr>
        <td><input type="password" name="pwd" value="<?php
          if(!empty($password))
            echo $password;
          ?>" />
          <?php
            if($error && empty($password))
              echo "<span class='errlabel'> please enter a password</span>";
          ?>
        </td>
      </tr>
    </table>


    <table>
      <tr>
        <td><input type="checkbox" name="remember" value="yes"/><label>Remember me</label></td>
      </tr>
      <tr>
        <td><?php if(!is_null($loginOK) && $loginOK==false) echo "<span class='errlabel'>Username and password do not match.</span>"; ?></td>
      </tr>
      <tr>
        <td><a href="createCustomerAcct.php">Don't have an account? Sign up</a>
      </tr>
      <tr>
        <td><input type="submit" name="submit" value="Login" /></td>
      </tr>
    </table>
  </form>

</body>
</html>
