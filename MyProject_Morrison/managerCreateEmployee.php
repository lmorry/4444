<?php
  $eUsername = "";
  $ePassword = "";
  $eEmail = "";
  $eShifts = 0;
  $err = false;

  if (isset($_POST["submit"])) {
      if(isset($_POST["eusername"])) $eUsername=$_POST["eusername"];
      if(isset($_POST["epassword"])) $ePassword=$_POST["epassword"];
      if(isset($_POST["eemail"])) $eEmail=$_POST["eemail"];
      if(isset($_POST["eshifts"])) $eShifts=$_POST["eshifts"];

      if(!empty($eUsername) && !empty($ePassword) && !empty($eEmail) && !empty($eShifts)) {
        header("HTTP/1.1 307 Temprary Redirect"); //
        header("Location: managerCompleteNewEmployee.php");
      } else {
        $err = true;
      }
  }
 ?>

 <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manager New Employee Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    function getTime(current) {
      var result = document.getElementById('time');
      setInterval(updateTime, 1000, false);

      function updateTime() {
        var curr = new Date();
        result.innerHTML = curr.toUTCString();
      }
    }
    document.addEventListener("DOMContentLoaded", getTime, false);
    </script>
    <style>
      .errlabel {color:red;}

      body {background-color:lightgrey}
      h1 {color:orange}
      h2 {color:orange}
      h3 {color:maroon}
      p {color:maroon}
      .nav-pills > li > a {color:maroon}
    </style>

  </head>
  <body>
    <div class="container-fluid">
      <h1>Manager New Employee Page</h1>
      <nav>
        <ul class="nav nav-pills">
          <li><a href="homepage.html">Home</a></li>
          <li><a href="managerMain.html">Main</a></li>
          <li class="active"><a href="managerCreateEmployee.php">New Employee</a></li>
          <li><a href="managerCurrentEmployees.php">Current Employees</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Schedule<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerAvailability.php">Employee Availability</a></li>
              <li><a href="managerMasterSchedule.php">Master Schedule</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"
            role="button" aria-haspopup="true" aria-expanded="false">Inventory<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="managerInventory.php">Inventory Items</a></li>
              <li><a href="managerOrderHistory.php">Order History</a></li>
            </ul>
          </li>
        </ul>
      </nav>

      <h2>Todays Date & Time: </h2>
      <h2 id="time"></h2>
      <img src="images/sharkeyslogo.jpg" style="width:30%">

    </div>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">

        <label>Employee Username</label>
        <input name="eusername" type="text" value="<?php echo $eUsername; ?>"/>
        <?php
          if ($err && empty($eUsername)) {
            echo "<label class='errlabel'>Error: Please enter an employee username</label>";
          }
        ?>
        <br />

        <label>Employee Password</label>
        <input name="epassword" type="password" value="<?php echo $ePassword; ?>"/>
        <?php
          if ($err && empty($ePassword)) {
            echo "<label class='errlabel'>Error: Please enter a password</label>";
          }
        ?>
        <br />

        <label>Employee Email</label>
        <input name="eemail" type="text" value="<?php echo $eEmail; ?>"/>
        <?php
          if ($err && empty($eEmail)) {
            echo "<label class='errlabel'>Error: Please enter an employee email</label>";
          }
        ?>
        <br />

        <label>Employee Shifts</label>
        <input type="number" name="eshifts" step="any" value="<?php echo $eShifts; ?>"/>
        <?php
          if ($err && $eShifts<=0) {
            echo "<label class='errlabel'>Error: Please enter the shifts</label>";
          }
        ?>
        <br />

        <input type="submit" name="submit" value="Submit" />
        <br />
      </form>
  </body>
</html>
